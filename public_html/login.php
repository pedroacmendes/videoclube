<?php

include 'conection.php';
include 'logsErros.php';

if (!empty($_POST)) {

  $email = ($_POST['email']);
  $password = ($_POST['password']);
  $session = $email;

  $stmt = $conn->prepare("SELECT * FROM utilizadores WHERE email = '$email'");
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows == 1) {
    while ($row = mysqli_fetch_array($result)) {

      if($row['conta'] == "Inativo"){
        echo '<script language="javascript" type="text/javascript"> alert("A sua conta encontra-se inativa, vá ao seu email para ativar a conta."); window.location.href="index.php" </script>';
      } else{

        if (password_verify($password, $row['password'])) {
          $_SESSION['email'] = $email;
          $_SESSION['tipoUser'] = $row['tipoUser'];
          if(isset($_POST['remember'])){
            $hour = time() + 3600 * 24 * 30;
            setcookie('email', $email, $hour);
            setcookie('password', $password, $hour);
          }
          wh_log("Login efetuado por $email");
          if($_SESSION['tipoUser'] == "User"){
            header('location:main.php');
          } else if($_SESSION['tipoUser'] == "Admin"){
            header('location:perfilAdmin.php');
          } else{
            wh_log("problema com tipo de utilizador; Conta: $email");
            echo '<script language="javascript" type="text/javascript"> alert("Surgiu algum problema com o seu tipo de utilizador"); window.location.href="index.php" </script>';
          }
        } else {        
          echo '<script language="javascript" type="text/javascript"> alert("Os seus dados de acesso estão errados."); window.location.href="index.php" </script>';
        }

      }
    }
  } else {
    session_unset();
    wh_log("Erro de login; Utilizador errado");
    echo '<script language="javascript" type="text/javascript"> alert("Os seus dados de acesso estão errados."); window.location.href="index.php" </script>';
    $stmt->close();
  }
}

?>