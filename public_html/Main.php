<!DOCTYPE html>
<html lang="en">

<?php 
include 'conection.php';
include 'head.php';
$utl = $_SESSION['email'];
if($utl == null){
  header('location:index.php');
}

?>

<body>

  <?php include 'navbarUser.php' ?>

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="/uploads/wal3.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Os melhores filmes da atualidade...</h3>
        </div>      
      </div>
      <div class="item">
        <img src="/uploads/2.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>No teu Clube de video</h3>
        </div>      
      </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container text-center">    
    <h2>Clube de Vídeo</h2><br>

    <div class="row">

      <table class="table table-striped table-hover">
       <tbody>

        <?php 

        $stmt = $conn->prepare("SELECT * FROM filmes");
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows === 0) exit('No rows');
        while($row = $result->fetch_assoc()) {
          $nome[] = $row['nome'];
          ?>
          <tr>
            <div class="col-sm-3">
              <p><?php echo "<img src='{$row['image']}' height='200' width='170' >"; ?></p>
              <p><?php echo $row['nome']; ?></p>  
            </div>
          </tr>
        <?php } $stmt->close(); ?>

      </tbody>       
    </table>

  </div>
</div>

</body>


<?php 
$conn ->close();
?>

</html>
