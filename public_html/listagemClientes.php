<!DOCTYPE html>
<html lang="en">

<?php 
include 'conection.php'; 
include 'head.php';

$utl = $_SESSION['email'];
if($utl == null){
	header('location:index.php');
}

?>

<body>

	<?php include 'navbarAdmin.php' ?>

	<div class="container-fluid">
		<div class="row content">
			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="perfilAdmin.php">Perfil</a></li>
					<li class="active"><a href="listagemClientes.php">Listagem de Clientes</a></li>
					<li><a href="manutencaoFilmes.php">Manutenção de Filmes</a></li>
					<li><a href="filmesAlugadosCliente.php">Filmes alugados</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9">

				<h1> Listagem de Clientes</h1><br>

				<div class="table-wrapper">
					<div class="table-title">		
						<a href='#editClienteModal' class="btn btn-warning"  data-toggle='modal'><span>Editar Cliente</span></a>
						<a href='#deleteClienteModal'  class="btn btn-danger" data-toggle='modal'><span>Eliminar Cliente</span></a>	
					</div>
				</div>

				<br><br>

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Foto</th>
							<th>Email</th>
							<th>Nome </th>
							<th>Tipo de Utilizador </th>
							<th>Telefone</th>
							<th>Estado conta</th>
							
						</tr>
					</thead>
					<tbody>
						<?php 

						$stmt = $conn->prepare("SELECT * FROM utilizadores order by tipoUser");
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows === 0) exit('No rows');
						while($row = $result->fetch_assoc()) {
							$email=$row["email"];
							?>
							<tr>
								<div class="col-sm-3">
									<td> <?php echo "<img src='{$row['image']}' height='100' width='70' >"; ?> </td>
									<td> <?php echo $row['email']; ?> </td>
									<td> <?php echo $row['nome']; ?> </td>
									<td> <?php echo $row['tipoUser']; ?> </td>
									<td> <?php echo $row['telefone']; ?> </td>
									<td> <?php echo $row['conta']; ?> </td> 
								</div>
							</tr>
						<?php } $stmt->close(); ?>

					</tbody>
				</table>

				<div id="editClienteModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form method="post">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Editar Tipo de utilizador</h4>
								</div>

								<div class="modal-body">					

									<div class="form-group">
										<label>Email</label>
										<input type="text" class="form-control"  name="email" required>
									</div>

									<div class="form-group">
										<label>Tipo de Utilizador</label> <br>
										<input type="radio" name="tipoUser" value="User"> Utilizador <br>
										<input type="radio" name="tipoUser" value="Admin"> Administrador <br>
									</div>

									<div class="modal-footer">
										<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
										<button type="submit" name="submit" class="btn btn-warning" value="UPLOAD" id="btnLogin">Editar</button>
									</div>


									<?php

									if(isset($_POST['submit'])){

										$email = $_POST["email"];
										$tipoUser = $_POST["tipoUser"];

										$stmt = $conn->prepare('UPDATE utilizadores SET tipoUser = ? WHERE email = ?');
										$stmt->bind_param('ss', $tipoUser, $email);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {
											echo '<script language="javascript" type="text/javascript"> alert("Cliente alterado com sucesso."); window.location.href="listagemClientes.php" </script>';
										} else {
											echo '<script language="javascript" type="text/javascript"> alert("Não foi possivel alterar."); window.location.href="listagemClientes.php" </script>';
										}
										$stmt->close();
									}

									?>

								</div>
							</form>
						</div>
					</div>	
				</div>


				<!-- Delete Modal HTML -->	
				<div id="deleteClienteModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form method="post">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Eliminar Cliente</h4>
								</div>
								<div class="modal-body">	
									<div class="form-group">
										<label>Email do Cliente</label>
										<input type="text" class="form-control"  name="email"required>
									</div>				
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
									<input type="hidden" name="eliminar" value="eliminado" />
									<button type="submit"  name = "upload" class="btn btn-danger" value = "UPLOAD" id="btnLogin">Eliminar</button>
								</div>

								<?php 
								if(isset($_POST['eliminar'])  == 'eliminado'){

									$email=$_POST["email"];

									$stmt = $conn->prepare('DELETE FROM utilizadores WHERE email = ?');
									$stmt->bind_param('s', $email);
									$stmt->execute();

									if ($stmt->affected_rows === 1) {
										echo '<script language="javascript" type="text/javascript"> alert("Cliente eliminado com sucesso."); window.location.href="listagemClientes.php" </script>';
									} else {
										echo '<script language="javascript" type="text/javascript"> alert("Não foi possivel eliminar o cliente."); window.location.href="listagemClientes.php" </script>';
									}
									$stmt->close();
								}

								?>

							</form>
						</div>
					</div>
				</div>	


			</div>
		</div>
	</div>


</body>
</html>