<!DOCTYPE html>
<html lang="en">


<?php 
include 'conection.php'; 
include 'head.php';
$utl = $_SESSION['email'];
if($utl == null){
	header('location:index.php');
}

?>
<body>

	<?php include 'navbarAdmin.php' ?>

	<div class="container-fluid">
		<div class="row content">

			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="perfilAdmin.php">Perfil</a></li>
					<li><a href="listagemClientes.php">Listagem de Clientes</a></li>
					<li class="active"><a href="manutencaoFilmes.php">Manutenção de Filmes</a></li>
					<li><a href="filmesAlugadosCliente.php">Filmes alugados</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9">

				<h1> Manutenção de Filmes</h1><br>

				<div class="table-wrapper">
					<div class="table-title">				
						<a href="#addFilmeModal" class="btn btn-success" data-toggle="modal"><span>Adicionar Filme</span></a>
						<a href='#editFilmModal' class="btn btn-warning"  data-toggle='modal'><span>Editar Filme</span></a>
						<a href='#deleteFilmModal'  class="btn btn-danger" data-toggle='modal'><span>Eliminar Filme</span></a>	
					</div>
				</div>

				<br><br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Foto</th>
							<th>Nome </th>
							<th>Disponibilidade</th>
							<th>Genero</th>
						</tr>
					</thead>
					<tbody>
						<?php 

						$stmt = $conn->prepare("SELECT * FROM filmes ");
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows === 0) exit('No rows');
						while($row = $result->fetch_assoc()) {
							$id=$row["nome"];
							?>
							<tr>
								<div class="col-sm-3">
									<td><?php echo "<img src='{$row['image']}' height='60' width='40' >"; ?></td>
									<td><?php echo $row['nome']; ?></td>
									<td><?php echo $row['disponibilidade']; ?></td> 
									<td><?php echo $row['genero']; ?></td> 
								</div>
							</tr>
						<?php } $stmt->close();?>

					</tbody>
				</table>    

				<!-- Edit Modal HTML -->
				<div id="addFilmeModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							

							<form class="form" role="form" enctype = "multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Inserir Filme</h4>
								</div>

								<div class="modal-body">					

									<div class="form-group">
										<label>Nome</label>
										<input type="text" class="form-control"  name="nome"required>
									</div>

									<div class="form-group">
										<label>Disponibilidade</label> <br>
										<input type="radio" name="disponibilidade" value="Disponivel" checked> Disponivel<br>
										<input type="radio" name="disponibilidade" value="Indisponivel"> Indisponivel<br>
										<input type="radio" name="disponibilidade" value="Brevemente"> Brevemente<br>
									</div>

									<div class="form-group">
										<label>Genero</label> <br>
										<input type="radio" name="genero" value="Acao" checked> Ação<br>
										<input type="radio" name="genero" value="Terror"> Terror<br>
										<input type="radio" name="genero" value="Romance"> Romance<br>
									</div>

									<div class="form-group">
										<label>Foto</label>
										<input type="file" id="image" name="image" />
									</div>					

									<div class="modal-footer">
										<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
										<button type="submit"  name = "upload" class="btn btn-success" value = "UPLOAD" id="btnLogin">Adicionar</button>
									</div>

									<?php

									if(isset($_POST["upload"])) {
										
										$filename = $_FILES["image"]["name"];	
										$tmpname = $_FILES["image"]["tmp_name"];	

										$image = "uploads/filmes/".$filename;

										move_uploaded_file($tmpname, $image);	

										$nome = $_POST['nome'];
										$disponibilidade = $_POST['disponibilidade'];
										$genero = $_POST['genero'];

										$stmt = $conn->prepare('INSERT INTO filmes VALUES(?,?,?,?)');
										$stmt->bind_param('ssss', $nome, $disponibilidade, $genero, $image);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {
											header('location:manutencaoFilmes.php');
											echo "<meta http-equiv='refresh' content='0'>";
										} else {
											echo "Error: " . $sql2 . "<br>" . $conn->error;
										}
										$stmt->close();						
									}

									?>
									
								</div>
							</form>
						</div>
					</div>
				</div>

				<!-- Edit Modal HTML -->
				<div id="editFilmModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form method="post">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Editar Filme</h4>
								</div>

								<div class="modal-body">					

									<div class="form-group">
										<label>Nome</label>
										<input type="text" class="form-control"  name="nome"required>
									</div>

									<div class="form-group">
										<label>Disponibilidade</label> <br>
										<input type="radio" name="disponibilidade" value="Disponivel"	> Disponivel<br>
										<input type="radio" name="disponibilidade" value="Indisponivel"> Indisponivel<br>
										<input type="radio" name="disponibilidade" value="Brevemente"> Brevemente<br>
									</div>

									<div class="form-group">
										<label>Genero</label> <br>
										<input type="radio" name="genero" value="Acao"> Ação<br>
										<input type="radio" name="genero" value="Terror"> Terror<br>
										<input type="radio" name="genero" value="Romance"> Romance<br>
									</div>

									<div class="modal-footer">
										<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
										<button type="submit" name="submit" class="btn btn-warning" value="UPLOAD" id="btnLogin">Editar</button>
									</div>


									<?php

									if(isset($_POST['submit'])){

										$nome = $_POST["nome"];
										$disponibilidade = $_POST["disponibilidade"];
										$genero = $_POST["genero"];

										$stmt = $conn->prepare('UPDATE filmes SET disponibilidade = ?, genero = ? WHERE nome = ?');
										$stmt->bind_param('sss', $disponibilidade, $genero, $nome);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {
											echo "New record created successfully";
											echo "<meta http-equiv='refresh' content='0'>";
										} else {
											echo "Error: " . $sql4 . "<br>" . $conn->error;
										}
										$stmt->close();
									}

									?>

								</div>
							</form>
						</div>
					</div>	
				</div>


				<!-- Delete Modal HTML -->	
				<div id="deleteFilmModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form method="post">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Eliminar Filme</h4>
								</div>
								<div class="modal-body">	
									<div class="form-group">
										<label>Nome do Filme</label>
										<input type="text" class="form-control"  name="nome"required>
									</div>				
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
									<input type="hidden" name="eliminar" value="eliminado" />
									<button type="submit"  name = "upload" class="btn btn-danger" value = "UPLOAD" id="btnLogin">Eliminar</button>
								</div>

								<?php 
								if(isset($_POST['eliminar'])  == 'eliminado'){

									$nome=$_POST["nome"];


									$stmt = $conn->prepare('DELETE FROM filmes WHERE nome = ?');
									$stmt->bind_param('s', $nome);
									$stmt->execute();

									if ($stmt->affected_rows === 1) {
										echo '<script language="javascript" type="text/javascript"> alert("Filme eliminado com sucesso."); window.location.href="manutencaoFilmes.php" </script>';
									} else {
										echo '<script language="javascript" type="text/javascript"> alert("Não foi possivel eliminar o filme."); window.location.href="manutencaoFilmes.php" </script>';
									}
									$stmt->close();
								}

								?>

							</form>
						</div>
					</div>
				</div>	


			</div>
		</div>
	</div>


</body>
</html>
