<style>
		.navbar {
			margin-bottom: 0;
			border-radius: 0;
		}

		footer {
			background-color: #2e2d2d;
			color: #FFFFFF;
			padding: 25px;
		}

		.carousel-inner img {
			width: 100%; /* Set width to 100% */
			margin: auto;
			min-height:200px;
		}

		@media (max-width: 600px) {
			.carousel-caption {
				display: none; 
			}
		}

		.meio{
			padding-left: 25px;
		}

		/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
		.row.content {height: 1000px}

		/* Set gray background color and 100% height */
		.sidenav {
			background-color: #e6e6e6;
			height: 100%;
		}

		/* On small screens, set height to 'auto' for the grid */
		@media screen and (max-width: 767px) {
			.row.content {height: auto;} 
		}

	</style>
