<html>


<?php 
include 'conection.php'; 
include 'head.php';

$utl = $_SESSION['email'];
if($utl == null){
	header('location:index.php');
}

?>

<body>

	<?php include 'navbarAdmin.php' ?>

	<div class="container-fluid">
		<div class="row content">

			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="perfilAdmin.php">Perfil</a></li>
					<li><a href="listagemClientes.php">Listagem de Clientes</a></li>
					<li><a href="manutencaoFilmes.php">Manutenção de Filmes</a></li>
					<li class="active"><a href="filmesAlugadosCliente.php">Filmes alugados</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9">

				<h1> Filmes alugados por cliente</h1><br>

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Email User</th>
							<th>ID</th>
							<th>Nome Filme </th>
							<th>Estado</th>
							<th>Data</th>
						</tr>
					</thead>
					<tbody>
						<?php 

						$stmt = $conn->prepare("SELECT * FROM filmesalugados order by emailUser");
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows === 0) exit('No rows');
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
								<div class="col-sm-3">
									<td><?php echo $row['emailUser']; ?></td>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['nomeFilme']; ?></td> 
									<td><?php echo $row['estado']; ?></td>
									<td><?php echo $row['data']; ?></td> 
								</div>
							</tr>
							<?php 
						}
						$stmt->close();
						?>

					</tbody>	
				</table>

			</div>	
		</div>
	</div>

</body>

</html>