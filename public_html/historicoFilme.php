<!DOCTYPE html>
<html lang="en">

<?php 
include 'conection.php'; 
include 'head.php'; 
$utl = $_SESSION['email'];
if($utl == null){
	header('location:index.php');
}
?>


<body>

	<?php include 'navbarUser.php' ?>

	<div class="container-fluid">
		<div class="row content">
			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="perfilUser.php">Perfil</a></li>
					<li><a href="requisitarFilme.php">Requisitar Filme</a></li>
					<li class="active"><a href="historicoFilme.php">Histórico</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9">

				<h1> Filmes alugados atualmente: </h1>

				<input type="submit" name="submit" class="btn btn-info"  data-toggle="modal" data-target="#modalDevolver" value="Devolver Filme"> 
				<br><br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nome Filme </th>
							<th>Email</th>
							<th>Estado</th>	
							<th>Data</th>							
						</tr>
					</thead>
					<tbody>
						<?php 
						
						$emailAtual = $_SESSION["email"];

						$stmt = $conn->prepare("SELECT * FROM filmesAlugados  WHERE emailUser = '$emailAtual' order by estado");
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows === 0) exit('No rows');
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['nomeFilme']; ?></td>
								<td><?php echo $row['emailUser']; ?></td> 
								<td><?php echo $row['estado']; ?></td> 
								<td><?php echo $row['data']; ?></td> 
								<td> </td>
							</tr>
						</tr>
						<?php
					}
					$stmt->close();
					?>

				</tbody>
			</table>

			<div id="modalDevolver" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="post">

							<div class="modal-header">						
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Devolver Filme</h4>
							</div>

							<div class="modal-body">					

								<div class="form-group">
									<label>Nome</label>
									<input type="text" class="form-control"  name="nome"required>
								</div>

								<div class="form-group" hidden>
									<input name="estado" value="Entregue">
								</div>

								<div class="modal-footer">
									<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
									<button type="submit" name="submit" class="btn btn-info" value="UPLOAD" id="btnLogin">Devolver</button>
								</div>


								<?php

								if(isset($_POST["submit"])) {

									$nome = $_POST["nome"];
									$estado = $_POST["estado"];

									$stmt = $conn->prepare('UPDATE filmesAlugados SET estado = ? WHERE nomeFilme = ? ');
									$stmt->bind_param('ss', $estado, $nome);
									$stmt->execute();

									if ($stmt->affected_rows === 1) {

										echo "New record created successfully";

										$nome = $_POST["nome"];
										$disponibilidade = $_SESSION['disponibilidade'];
										$situation = "Disponivel";

										$stmt = $conn->prepare('UPDATE filmes SET disponibilidade = ? WHERE nome = ? ');
										$stmt->bind_param('ss', $situation, $nome);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {
											echo "New record created successfully";
										} else {
											echo "Erro: " . $sql . "<br>" . $conn->error;
										}
										echo "<meta http-equiv='refresh' content='0'>";		
										$stmt->close();
									} else {
										echo '<script language="javascript" type="text/javascript"> alert("Não foi possivel devolver o filme."); window.location.href="historicoFilme.php" </script>';
									}
									$stmt->close();

								}

								?>   

							</div>
						</form>
					</div>
				</div>	
			</div>

		</div>
	</div>

</body>

</html>