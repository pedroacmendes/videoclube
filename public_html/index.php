<!DOCTYPE html>
<html lang="en">

<?php 
include 'conection.php';
include 'head.php'; 
include 'logsErros.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include 'bdTabelas.php';
criarTabelaFilmes($conn);
criarTabelaUtilizadores($conn);
criarTabelaFilmesAlugados($conn);

?>

<body>

  <nav class="navbar navbar-inverse">
    <div class="collapse navbar-collapse" id="myNavbar">

      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php"><img src="uploads/home.png" width="18" height="18"/> VideoClube</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li data-toggle="modal" data-target="#modalRegistar"><a href="#"><img src="uploads/sign-up.png" width="20" height="18" />Registo</a></li>
        <li data-toggle="modal" data-target="#modalLogin"><a href="#"><span class="glyphicon glyphicon-log-in"> </span> Login</a></li>
        
        <div id="modalLogin" class="modal fade" role="dialog">
          <div class="modal-dialog ">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login</h4>
              </div>

              <div class="modal-body">

                <form class="form-horizontal" method="post" action="/login.php">

                  <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password:</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label><input type="checkbox" name="remember" id="remember" value="1" checked> Remember me</label>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <a data-toggle="modal" data-target="#modalEsqieciPassword" class="btn btn-default" href="#"> Esqueci password </a>
                    <button type="submit" id="Entrar" class="btn btn-success">Entrar</button>
                  </div>

                </form>

              </div>
            </div>
          </div>
        </div>

        <div id="modalEsqieciPassword" class="modal fade" role="dialog">
          <div class="modal-dialog ">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recuperação de conta</h4>
              </div>

              <div class="modal-body">

                <form class="form-horizontal" method="post">
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
                    </div>
                  </div>

                  <div class="modal-footer">
                    <button type="submit" name="submit" id="Entrar" class="btn btn-success">Enviar email</button>
                  </div>

                  <?php

                  if(isset($_POST["submit"])) {

                    $email=$_POST['email'];

                    require './phpmailer/vendor/autoload.php';
//Create a new PHPMailer instance
                    $mail = new PHPMailer;
//Charset for the message
                    $mail->CharSet = 'utf-8';
//Tell PHPMailer to use SMTP
                    $mail->isSMTP();
//Enable SMTP debugging
                    $mail->SMTPDebug = 2; 
//0 = off (for production use); 1 = client messages; 2 = client and server messages
//Set the hostname of the mail server
                    $mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
                    $mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
                    $mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
                    $mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
                    $mail->Username = "pedroacmendesprofissional@gmail.com";
//Password to use for SMTP authentication
                    $mail->Password = "segurancaSAW";
//Set who the message is to be sent from
                    $mail->setFrom('seg.apli.web@gmail.com', 'Segurança das Aplicações Web');
//Set who the message is to be sent to
                    $mail->addAddress($email, 'NomeDestino');
//Set the subject line
                    $mail->Subject = 'SAW.com';
//allow html code in body message
                    $mail->IsHTML(true);
//Body text for the message
                    $mail->Body = " Para recuperar a password carrega no seguinte link:


                    http://saw_8180666_8180384.com/recuperaPassword.php"; 

//aditional options to SMTP

                    $mail->SMTPOptions = array('ssl' => array('verify_peer' => false,'verify_peer_name' => false,'allow_self_signed' => true));
                    if (!$mail->send()) {
                      echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                      echo "Message sent!";
                      echo "<meta http-equiv='refresh' content='0'>";
                    }
                  }

                  ?>

                </form>
              </div>
            </div>
          </div>
        </div>
      </ul>
    </div>
  </nav>

  <div id="modalRegistar" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">

        <form class="form" role="form" enctype = "multipart/form-data" autocomplete="off" id="formRegisto" novalidate="" method="POST">

          <div class="modal-header">          
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Registar Utilizador</h4>
          </div>

          <div class="modal-body">          

            <div class="form-group">
              <input type="hidden" name="tipoUser" value="User"> 
            </div>

            <div class="form-group">
              <label>Nome</label>
              <input type="text" class="form-control" name="nome" required>
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="text" class="form-control" name="email" required>
            </div>

            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password" required>
            </div>

            <div class="form-group">
              <label>Telefone</label>
              <input type="text" class="form-control" name="telefone" required>
            </div> 

            <div class="form-group">
              <label>Foto</label>
              <input type="file" id="image" name="image" >
            </div>

            <div class="modal-footer">
             <button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancelar</button>
             <button type="submit" name="upload" value="UPLOAD" id="btnRegisto" class="btn btn-success">Adicionar</button>
           </div>

           <?php

           if(isset($_POST["upload"])) {

            $filename = $_FILES['image']['name']; 
            $tmpname = $_FILES["image"]["tmp_name"];  

            $image = "uploads/users/".$filename;

            move_uploaded_file($tmpname, $image);

            $validate = "false";

            if($validate == 'false'){

              $tipoUser=$_POST['tipoUser'];
              $cod = rand(1,100);
              $nome=$_POST['nome'];
              $email=$_POST['email'];
              $telefone = $_POST['telefone'];
              $password=$_POST['password'];
              $uppercase = preg_match('@[A-Z]@', $password);
              $lowercase = preg_match('@[a-z]@', $password);
              $number = preg_match('@[0-9]@', $password);
              $specialChars = preg_match('@[^\w]@', $password);

              if(!preg_match("/^[a-zA-Z ]*$/", $nome)) {
                echo '<script language="javascript" type="text/javascript"> alert("Por favor digite um nome correto. (Só letras)"); window.location.href="index.php" </script>';
              } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
               echo '<script language="javascript" type="text/javascript"> alert("Por favor digite um email correto."); window.location.href="index.php" </script>';
                } /* else if (substr(trim($telefone),0,2) !== '91' or '92'  or '93'  or '96'  or '255'){
                  echo '<script language="javascript" type="text/javascript"> alert("Por favor digite um numero de telefone correto."); window.location.href="index.php" </script>';
                } */ else {  

                  $passEncript = password_hash($password, PASSWORD_DEFAULT);
                  $conta = "Inativo";

                  $stmt = $conn->prepare('INSERT INTO utilizadores VALUES(?,?,?,?,?,?,?,?)');
                  $stmt->bind_param('sssissis', $nome, $passEncript, $tipoUser, $telefone, $email, $image , $cod, $conta );
                  $stmt->execute();


                  if ($stmt->affected_rows === 1) {

                    require './phpmailer/vendor/autoload.php';
//Create a new PHPMailer instance
                    $mail = new PHPMailer;
//Charset for the message
                    $mail->CharSet = 'utf-8';
//Tell PHPMailer to use SMTP
                    $mail->isSMTP();
//Enable SMTP debugging
                    $mail->SMTPDebug = 2; 
//0 = off (for production use); 1 = client messages; 2 = client and server messages
//Set the hostname of the mail server
                    $mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
                    $mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
                    $mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
                    $mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
                    $mail->Username = "pedroacmendesprofissional@gmail.com";
//Password to use for SMTP authentication
                    $mail->Password = "segurancaSAW";
//Set who the message is to be sent from
                    $mail->setFrom('seg.apli.web@gmail.com', 'Segurança das Aplicações Web');
//Set who the message is to be sent to
                    $mail->addAddress($email, 'NomeDestino');
//Set the subject line
                    $mail->Subject = 'SAW.com';
//allow html code in body message
                    $mail->IsHTML(true);
//Body text for the message
                    $mail->Body = " Obrigado pelo registo 

                    Username: '$email'
                    Password: '$password'

                    Cique no link para ativar a conta: <br>

                    http://saw_8180666_8180384.com/verificaemail.php?email=$email&cod=$cod ";
//aditional options to SMTP

                    $mail->SMTPOptions = array('ssl' => array('verify_peer' => false,'verify_peer_name' => false,'allow_self_signed' => true));
                    if (!$mail->send()) {
                      echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                      echo "Message sent!";
                      echo "<meta http-equiv='refresh' content='0'>";
                    }
              //header ('location:main.php');

                  } 
                  else {
                    echo "Erro: " . $sql . "<br>" . $conn->error;
                  }
                  $stmt->close();
                  wh_log("Registo efetuado; Utilizador novo");
                }
              }
            }

            ?>   

          </div>

        </form>

      </div>
    </div>
  </div>

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="/uploads/wal3.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Os melhores filmes da atualidade...</h3>
        </div>      
      </div>
      <div class="item">
        <img src="/uploads/1.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>No teu Clube de video</h3>
        </div>      
      </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container text-center">    
    <h2>Clube de Vídeo</h2><br>
    <div class="row">
      <table class="table table-striped table-hover">
        <tbody>

          <?php 

          $stmt = $conn->prepare("SELECT * FROM filmes");
          $stmt->execute();
          $result = $stmt->get_result();
          if($result->num_rows === 0) exit('No rows');
          while($row = $result->fetch_assoc()) {
            $nome[] = $row['nome'];
            ?>
            <tr>
              <div class="col-sm-3">
                <p><?php echo "<img src='{$row['image']}' height='200' width='170' >"; ?></p>
                <p><?php echo $row['nome']; ?></p>  
              </div>
            </tr>
          <?php } $stmt->close();  ?>

        </tbody>
      </table>
    </div>
  </div>

</body>

<footer>

 <h2>Quer alugar um filme?</h2><br>

 <p> Deseja alugar um filme e não sabe como o fazer? <br> 
  Não se preocupe o seu problema foi resolvido. Ao pressionar o seguinte botão será convidado a registar se no site para dessa forma ter acesso a todos os filmes disponiveis e novidades de ultima hora. <br>
  Venha fazer parte! Junte se ao grupo!  </p>

  <button data-toggle="modal" data-target="#modalRegistar"><a href="#"><img src="uploads/sign-up.png" width="20" height="18"/> Registo </a></button>

</footer> 


<?php $conn=null; ?>

</html>