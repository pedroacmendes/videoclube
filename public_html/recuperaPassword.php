<html>

<?php

error_reporting(0); //sao macumbas
include 'conection.php'; 
include 'head.php'; 

?>

<style type="text/css">

	body{	
		background-color: #2e2d2d;
	}	

</style>

<body>

	<div class="modal-dialog ">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Recuperação de conta</h4>
			</div>

			<div class="modal-body">
				<form class="form-horizontal" method="post">

					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="email" name="email" placeholder="Enter email">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="password">Password:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
						</div>
					</div>

					<div class="modal-footer">
						<button type="submit" name="submit" id="submit" class="btn btn-success"> Renovar password</button>
					</div>

					<?php

					if(isset($_POST["submit"])) {

						$email = $_POST['email'];
						$password=$_POST['password'];
						$uppercase = preg_match('@[A-Z]@', $password);
						$lowercase = preg_match('@[a-z]@', $password);
						$number = preg_match('@[0-9]@', $password);
						$specialChars = preg_match('@[^\w]@', $password);
						$passEncript = password_hash($password, PASSWORD_DEFAULT);

						$stmt = $conn->prepare('UPDATE utilizadores SET password = ? WHERE email = ? ');
						$stmt->bind_param('ss', $passEncript, $email);
						$stmt->execute();

						if ($stmt->affected_rows === 1) {
							echo "<script language='javascript' type='text/javascript'>alert('Password alterada com sucesso!'); window.location.href='index.php'</script>";
						} else {
							echo "<script language='javascript' type='text/javascript'>alert('Password não foi alterada surgiu um erro!'); window.location.href='index.php'</script>";
						}	
						$stmt->close();			
					}

					?>

				</form>
			</div>
		</div>
	</div>


</body>
</html>