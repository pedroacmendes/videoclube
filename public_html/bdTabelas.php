<?php

function criarBD ($conn){
  $sql = "CREATE DATABASE IF NOT EXISTS clubevideo";
}

function criarTabelaFilmes($conn){
  $sql = "CREATE TABLE IF NOT EXISTS filmes (
  nome varchar(50) NOT NULL,
  disponibilidade varchar(20) NOT NULL,
  genero varchar(100) NOT NULL,
  image varchar(200) NOT NULL)"; 
  if($conn->query($sql) != TRUE) 
    echo "Erro na criação da tabela: " . $conn -> error;
}

function criarTabelaUtilizadores($conn){
  $sql = "CREATE TABLE IF NOT EXISTS utilizadores (
  nome varchar(50) NOT NULL,
  password varchar(90) NOT NULL,
  tipoUser varchar(20) NOT NULL,
  telefone int(15) NOT NULL, 
  email varchar(50) NOT NULL PRIMARY KEY,
  image varchar(200) NOT NULL,
  cod varchar(200) NOT NULL,
  conta varchar(200) NOT NULL)";
  if($conn->query($sql) != TRUE) 
    echo "Erro na criação da tabela: " . $conn -> error;
}

function criarTabelaFilmesAlugados($conn){
  $sql = "CREATE TABLE IF NOT EXISTS filmesalugados  (
  id int(200) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nomeFilme varchar(200) NOT NULL,
  emailUser varchar(200) NOT NULL,
  estado varchar(200) NOT NULL,
  data timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  FOREIGN KEY (nomeFilme) REFERENCES filmes (nome),
  FOREIGN KEY (emailUser) REFERENCES utilizadores (email) )";
if($conn->query($sql) != TRUE) 
  echo "Erro na criação da tabela: " . $conn -> error;
}

?>