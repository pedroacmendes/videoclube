<!DOCTYPE html>
<html lang="en">

<?php include 'conection.php' ?>
<?php include 'head.php' ?>

<style>
	.grid-container {
		display: grid;
		grid-template-columns: 250px 250px;
		grid-gap: 10px;
		padding: 10px;
	}

	.grid-container > div {
		text-align: center;
		padding: 20px 0;
		font-size: 20px;
	}

	table {
		width: 100%;
		height: 100%;
	}

	td, th {

		text-align: left;
		padding: 8px;
	}


</style>

<body>

	<?php include 'navbarUser.php' ?>

	<div class="container-fluid">
		<div class="row content">

			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="perfilUser.php">Perfil</a></li>
					<li><a href="requisitarFilme.php">Requisitar Filme</a></li>
					<li><a href="historicoFilme.php">Histórico</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9">

				<h1 style="padding-left: 60px;"> Meu Perfil</h1>

				<?php 

				$utl = $_SESSION['email'];
				if($utl == null){
					header('location:index.php');
				}

				$stmt = $conn->prepare("SELECT * FROM utilizadores  WHERE email = '$utl'");
				$stmt->execute();
				$result = $stmt->get_result();
				if($result->num_rows === 1){
					$row = mysqli_fetch_array($result);
				}

				?>	

				<div class="grid-container">
					<div>
						<?php echo "<img src='{$row['image']}' class='figure-img img-fluid rounded' height='200' width='170'>"."<br>"; ?>
					</div>
					<div>

						<table>							
							<tr>
								<td>Nome: <?php echo $row['nome'] ?></td>
							</tr>
							<tr>
								<td>Email:<?php echo $row['email'] ?></td>
							</tr>
							<tr>
								<td>Telefone: <?php echo $row['telefone'] ?></td>
							</tr>
						</table>


					</div>
				</div>

				<div class="grid-container">
					<button href='#editarUser' class="btn btn-success" data-toggle='modal'>Alterar</button>
					<button href='#eliminarUser' class="btn btn-danger" data-toggle='modal'>Eliminar</button>
				</div>


				<div id="editarUser" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form class="form" role="form" enctype = "multipart/form-data" autocomplete="off" id="formEditar" novalidate="" method="POST">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Editar Perfil</h4>
								</div>

								<div class="modal-body">					

									<div class="form-group">
										<label>Nome</label>
										<input type="text" class="form-control"  name="nome"required>
									</div>

									<div class="form-group">
										<label>Password</label>
										<input type="password" class="form-control"  name="password"required>
									</div>

									<div class="form-group">
										<label>Telefone</label>
										<input type="text" class="form-control"  name="telefone"required>
									</div>

									<div class="form-group">
										<label>Fotografia</label>
										<input type="file" class="form-control"  name="image" >
									</div>

									<div class="modal-footer">
										<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
										<input type="submit" name="submit" class="btn btn-info" value="Guardar">
									</div>


									<?php

									if(isset($_POST['submit'])){

										$email = $_SESSION['email'];
										$nome = $_POST['nome'];
										$telefone = $_POST['telefone'];
										$filename = $_FILES['image']['name'];	
										$tmpname = $_FILES['image']['tmp_name'];	
										$image = "uploads/users/".$filename;
										move_uploaded_file($tmpname, $image);

										$password=$_POST['password'];
										$uppercase = preg_match('@[A-Z]@', $password);
										$lowercase = preg_match('@[a-z]@', $password);
										$number = preg_match('@[0-9]@', $password);
										$specialChars = preg_match('@[^\w]@', $password);
										$passEncript = password_hash($password, PASSWORD_DEFAULT);

										$stmt = $conn->prepare('UPDATE utilizadores SET nome = ?, password = ?, telefone = ?, image = ? WHERE email = ?');
										$stmt->bind_param('ssiss', $nome, $passEncript, $telefone, $image, $email);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {
											echo "New record created successfully";
											echo "<meta http-equiv='refresh' content='0'>";
										} else {
											echo '<script language="javascript" type="text/javascript"> alert("Alguma coisa não está correta. Por favor tente novamente."); window.location.href="perfilUser.php" </script>';
										}
										$stmt->close();
									}

									?>

								</div>
							</form>
						</div>
					</div>	
				</div>

				<div id="eliminarUser" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form method="post">

								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Eliminar Perfil</h4>
								</div>

								<div class="modal-body">					

									<div class="modal-body">					

										<div class="form-group">
											<label>Email</label>
											<input type="text" class="form-control" name="email" required>
										</div>

										<div class="modal-footer">
											<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
											<input type="submit" name="eliminar" id="eliminar" class="btn btn-danger" value="Eliminar">
										</div>

										<?php

										if(isset($_POST['eliminar'])){

											$email = $_POST['email'];

											$stmt = $conn->prepare('DELETE FROM filmesalugados WHERE emailUser = ?');
											$stmt->bind_param('s', $email);
											$stmt->execute();
											$stmt->close();

											$stmt = $conn->prepare('DELETE FROM utilizadores WHERE email = ?');
											$stmt->bind_param('s', $email);
											$stmt->execute();

											if ($stmt->affected_rows === 1) {
												echo "<meta http-equiv='refresh' content='0; url=index.php'>";
											} else {
												echo '<script language="javascript" type="text/javascript"> alert("Alguma coisa não está correta. Por favor tente novamente."); window.location.href="perfilUser.php" </script>';
											}
											$stmt->close();
										}

										?>

									</div>

								</div>
							</form>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>

</body>

</html>