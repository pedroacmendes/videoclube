<?php 
include 'conection.php'; 
include 'head.php'; 
$utl = $_SESSION['email'];
if($utl == null){
  header('location:index.php');
}
?>


<body>

  <?php include 'navbarUser.php' ?>

  <div class="container text-center">    

    <h2>Filmes disponiveis </h2> <br>

    <div class="row">

      <table class="table table-striped table-hover">

       <tbody>

        <?php 

        $stmt = $conn->prepare("SELECT * FROM filmes WHERE disponibilidade = 'Disponivel'");
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows === 0) exit('No rows');
        while($row = $result->fetch_assoc()) {
          $nome[] = $row['nome'];
          ?>
          <tr>
            <div class="col-sm-3">
              <p><?php echo "<img src='{$row['image']}' height='200' width='170' >"; ?></p>
              <p><?php echo $row['nome']; ?></p>
              <p><?php echo $row['genero']; ?></p>  
              <p><?php echo $row['disponibilidade']; ?></p>   
            </div>
          </tr>
          <?php 
        }
        $stmt->close();
        ?>

      </tbody>      
    </table>

  </div>
  
</div>

</body>