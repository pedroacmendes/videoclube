<!DOCTYPE html>
<html lang="en">

<?php 
include 'conection.php'; 
include 'head.php'; 
include 'logsErros.php';
$utl = $_SESSION['email'];
if($utl == null){
	header('location:index.php');
}
?>

<body>

	<?php include 'navbarUser.php' ?>

	<div class="container-fluid">
		<div class="row content">

			<div class="col-sm-3 sidenav hidden-xs">
				<h2>Menu</h2>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="perfilUser.php">Perfil</a></li>
					<li class="active"><a href="requisitarFilme.php">Requisitar Filme</a></li>
					<li><a href="historicoFilme.php">Histórico</a></li>
				</ul><br>
			</div>

			<div class="col-sm-9"> 

				<h1> Requisitar </h1> <br>

				<input type="submit" name="submit" class="btn btn-info"  data-toggle="modal" data-target="#modalRequisitar" value="Requisitar"> 
				<br><br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Foto</th>
							<th>Nome </th>
							<th>Disponibilidade</th>
							<th>Genero</th>							
						</tr>
					</thead>
					<tbody>
						<?php 

						$stmt = $conn->prepare("SELECT * FROM filmes  WHERE disponibilidade = 'Disponivel'");
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows === 0) exit('No rows');
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
								<td><?php echo "<img src='{$row['image']}' height='60' width='40'>"; ?></td>
								<td><?php echo $row['nome']; ?></td>
								<td><?php echo $row['disponibilidade']; ?></td> 
								<td><?php echo $row['genero']; ?></td> 
								<td> </td>
							</tr>
						</tr>
					<?php } $stmt->close();?>

				</tbody>
			</table>

			<div id="modalRequisitar" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">

						<form class="form" role="form" enctype = "multipart/form-data" autocomplete="off" id="formRegisto" novalidate="" method="POST">

							<div class="modal-header">          
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Requisitar Filme</h4>
							</div>

							<div class="modal-body">          

								<div class="form-group">
									<label>Digite o nome do filme que pretende requisitar: </label>
								</div>

								<div class="form-group">
									<input name="nome" class="form-control">
								</div>

								<div class="modal-footer">
									<button data-dismiss="modal" class="btn btn-warning" aria-hidden="true"> Não </button>
									<button type="submit" name="submit" class="btn btn-success"> Sim </button>
								</div>

								<?php

								if(isset($_POST["submit"])) {

									$email = $_SESSION['email'];
									$nome = $_POST["nome"];
									$disponibilidade = "Indisponivel";

									$stmt = $conn->prepare("SELECT * FROM filmes WHERE nome = '$nome' AND disponibilidade = 'Disponivel'");
									$stmt->execute();
									$result = $stmt->get_result();
									if($result->num_rows === 1){

										$stmt = $conn->prepare('UPDATE filmes SET disponibilidade = ? WHERE nome = ? ');
										$stmt->bind_param('ss', $disponibilidade, $nome);
										$stmt->execute();

										if ($stmt->affected_rows === 1) {

											$nomeFilme = $_POST["nome"];
											$emailUser = $_SESSION['email'];
											$estado ="Alugado";

											$sql = "INSERT INTO filmesalugados VALUES ('', '$nomeFilme','$emailUser', '$estado', '')";
											if ($conn->query($sql) === TRUE) 
												echo "<meta http-equiv='refresh' content='0'>";
										} 
									} else{
										wh_log("ERRO ao requisitar filme; Reserva existente/Indisponivel"); 
										echo '<script language="javascript" type="text/javascript"> alert("Por favor digite um filme existente e disponivel."); window.location.href="requisitarFilme.php" </script>';
									}
									$stmt->close();
								}

								?>   

							</div>

						</form>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>

</body>


</html>

